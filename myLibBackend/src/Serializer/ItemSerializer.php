<?php
namespace App\Serializer;

use App\Entity\Item;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class ItemSerializer
{
    private User $user;

    public function __construct(Security $security) {
        $this->user = $security->getUser();
    }

    public function serializeItem(Item $item): array
    {
        $wishlisted = false;

        foreach ($item->getWishlistedItems() as $wishlisted) {
            if ($wishlisted->getUser() === $this->user) {
                $wishlisted = true;
            }
        }

        return [
            'id' => $item->getId(),
            'title' => $item->getTitle(),
            'author' => $item->getAuthor(),
            'bookCondition' => $item->getBookCondition(),
            'price' => $item->getPrice(),
            'createdAt' => $item->getCreatedAt()->format('Y-m-d'),
            'owned' => $item->getSeller() == $this->user,
            'wishlisted' => $wishlisted,
            'numberOfWishlists' => count($item->getWishlistedItems()),
            'user' => [
                'id' => $item->getSeller()->getId(),
                'username' => $item->getSeller()->getUsername(),
                'phoneNumber' => $item->getSeller()->getPhoneNumber()
            ]
        ];
    }
}