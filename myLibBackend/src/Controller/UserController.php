<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;

/**
 * @Route("/api", name="api_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(ManagerRegistry $doctrine, Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        $requestData = json_decode($request->getContent(), true);
        if (!isset($requestData['email']) || !isset($requestData['username']) || !isset($requestData['password'])) {
            return $this->json(['message' => 'Invalid registration data.'], 400);
        }
        $email = $requestData['email'];
        $username = $requestData['username'];
        $password = $requestData['password'];
        
        $user = new User();
        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            $password
        );
        
        $user->setEmail($email)
            ->setUsername($username)
            ->setPassword($hashedPassword);

        $em = $doctrine->getManager();
        $em->persist($user);
        $em->flush();
  
        return $this->json([], 201);
    }

    /**
     * @Route("/profile", name="user_profile", methods={"GET"})
     */
    public function profile()
    {
        // TODO: return current user's data and their wishlisted items
    }
}
