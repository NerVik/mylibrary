<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

use App\Entity\Item;
use App\Entity\Sale;
use App\Serializer\ItemSerializer;

/**
 * @Route("/api", name="api_")
 */
class ItemController extends AbstractController
{
    /**
     * @Route("/item", name="item_create", methods={"POST"})
     */
    public function newItem(ManagerRegistry $doctrine, Request $request, Security $security): Response
    {
        $requestData = json_decode($request->getContent(), true);

        if (!isset($requestData['title']) ||
            !isset($requestData['author']) ||
            !isset($requestData['price']) ||
            !isset($requestData['bookCondition'])
        ) {
            return $this->json(['message' => 'Invalid request data.'], 400);
        }

        $item = new Item();
        $item->setTitle($requestData['title'])
            ->setAuthor($requestData['author'])
            ->setPrice($requestData['price'])
            ->setBookCondition($requestData['bookCondition'])
            ->setSeller($security->getUser());

        $em = $doctrine->getManager();
        $em->persist($item);
        $em->flush();

        return $this->json([], 201);
    }

    /**
     * @Route("/item", name="item_list", methods={"GET"})
     */
    public function list(ManagerRegistry $doctrine, Request $request, ItemSerializer $itemSerializer): Response
    {
        $requestData = json_decode($request->getContent(), true);

        $queryBuilder = $doctrine->getManager()->createQueryBuilder();
        $queryBuilder->select('i')
            ->from('App\Entity\Item', 'i')
            ->leftJoin('i.sales', 's', 'WITH', 's.status = :soldStatus')
            ->where('s.id IS NULL')
            ->setParameter('soldStatus', Sale::STATUS_SOLD);
        $queryBuilder = $this->addQueryConditions($queryBuilder, $requestData);
        $queryBuilder = $this->addOrderBy($queryBuilder, $requestData);
        
        $items = $queryBuilder->getQuery()->getResult();

        $data = [];
        foreach ($items as $item) {
            $data[] = $itemSerializer->serializeItem($item);
        }

        return $this->json($data, 200);
    }

    private function addQueryConditions(QueryBuilder $queryBuilder, array $requestData): QueryBuilder
    {
        if (isset($requestData['title'])) {
            $queryBuilder->andWhere('LOWER(i.title) LIKE LOWER(:title)')
                ->setParameter('title', $requestData['title']);
        }

        if (isset($requestData['author'])) {
            $queryBuilder->andWhere('LOWER(i.author) LIKE LOWER(:author)')
                ->setParameter('author', $requestData['author']);
        }

        return $queryBuilder;
    }

    private function addOrderBy(QueryBuilder $queryBuilder, array $requestData): QueryBuilder
    {
        if (!isset($requestData['orderByProperty']) || !isset($requestData['orderByDirection'])) {
            return $queryBuilder;
        }

        if ($requestData['orderByProperty'] !== 'createdAt' || $requestData['orderByProperty'] !== 'price') {
            return $queryBuilder;
        }

        if ($requestData['orderByDirection'] !== 'ASC' || $requestData['orderByDirection'] !== 'DESC') {
            return $queryBuilder;
        }

        $queryBuilder->orderBy('i.' . $requestData['orderByProperty'], $requestData['orderByDirection']);
        
        return $queryBuilder;
    }

    /**
     * @Route("/item/{id}", name="item_single", methods={"GET"})
     */
    public function getSingle(ManagerRegistry $doctrine, int $id, ItemSerializer $itemSerializer): Response
    {
        $item = $doctrine->getRepository(Item::class)->find($id);

        if (!$item) {
            return $this->json(['message' => 'Item not found.'], 404);
        }

        return $this->json($itemSerializer->serializeItem($item), 200);
    }

    /**
     * @Route("/item/{id}", name="item_delete", methods={"DELETE"})
     */
    public function delete(ManagerRegistry $doctrine, int $id, Security $security): Response
    {
        $item = $doctrine->getRepository(Item::class)->find($id);

        if (!$item) {
            return $this->json(['message' => 'Item not found.'], 404);
        }

        if ($item->getSeller() !== $security->getUser()) {
            return $this->json([], 403);
        }

        $em = $doctrine->getManager();
        $em->remove($item);
        $em->flush();

        return $this->json([], 202);
    }

    /**
     * @Route("/item/{id}", name="item_edit", methods={"PUT"})
     */
    public function edit(ManagerRegistry $doctrine, Request $request, int $id, Security $security): Response
    {
        $requestData = json_decode($request->getContent(), true);

        $item = $doctrine->getRepository(Item::class)->find($id);

        if ($item->getSeller() !== $security->getUser()) {
            return $this->json([], 403);
        }

        if (!$item) {
            return $this->json(['message' => 'Item not found.'], 404);
        }

        if (!isset($requestData['title']) ||
            !isset($requestData['author']) ||
            !isset($requestData['price']) ||
            !isset($requestData['bookCondition'])
        ) {
            return $this->json(['message' => 'Invalid request data.'], 400);
        }

        $item->setTitle($requestData['title'])
            ->setAuthor($requestData['author'])
            ->setPrice($requestData['price'])
            ->setBookCondition($requestData['bookCondition']);

        $em = $doctrine->getManager();
        $em->persist($item);
        $em->flush();

        return $this->json([], 200);
    }
}
