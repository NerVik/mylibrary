<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

use App\Entity\Item;
use App\Entity\WishlistedItem;
use App\Serializer\ItemSerializer;

/**
 * @Route("/api", name="api_")
 */
class WishlistController extends AbstractController
{
    /**
     * @Route("/wishlist", name="wishlist_create", methods={"POST"})
     */
    public function newWishlist(ManagerRegistry $doctrine, Request $request, Security $security): Response
    {
        $requestData = json_decode($request->getContent(), true);

        if (!isset($requestData['itemId'])) {
            return $this->json(['message' => 'Invalid request data.'], 400);
        }

        $item = $doctrine->getRepository(Item::class)->find($requestData['itemId']);

        if (!$item) {
            return $this->json(['message' => 'Item not found.'], 400);
        }

        $wishlistedItem = $doctrine->getRepository(WishlistedItem::class)->findBy([
            'user' => $security->getUser(),
            'item' => $item
        ]);

        if ($wishlistedItem) {
            return $this->json(['message' => 'Already wishlisted.'], 400);
        }

        $wishlistedItem = new WishlistedItem();
        $wishlistedItem->setUser($security->getUser())
            ->setItem($item);

        $em = $doctrine->getManager();
        $em->persist($wishlistedItem);
        $em->flush();

        return $this->json([], 201);
    }

    /**
     * @Route("/wishlist/{id}", name="wishlist_delete", methods={"DELETE"})
     */
    public function delete(ManagerRegistry $doctrine, int $id, Security $security): Response
    {
        $wishlistedItem = $doctrine->getRepository(WishlistedItem::class)->find($id);

        if (!$wishlistedItem) {
            return $this->json(['message' => 'Non-existant id.'], 400);
        }

        if ($wishlistedItem->getUser() !== $security->getUser()) {
            return $this->json(['message' => 'You are not the owner of this listing.'], 403);
        }

        $em = $doctrine->getManager();
        $em->remove($wishlistedItem);
        $em->flush();

        return $this->json([], 200);
    }
}