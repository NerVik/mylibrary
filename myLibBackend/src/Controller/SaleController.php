<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Doctrine\Persistence\ManagerRegistry;

use App\Entity\Item;
use App\Entity\Sale;

/**
 * @Route("/api", name="api_")
 */
class SaleController extends AbstractController
{
    /**
     * @Route("/sale", name="sale_create", methods={"POST"})
     */
    public function newSale(ManagerRegistry $doctrine, Request $request, Security $security): Response
    {
        $requestData = json_decode($request->getContent(), true);

        if (!isset($requestData['itemId'])) {
            return $this->json(['message' => 'Invalid request data.'], 400);
        }

        $item = $doctrine->getRepository(Item::class)->find($requestData['itemId']);

        if (!$item) {
            return $this->json(['message' => 'Item does not exist.'], 400);
        }
        if ($item->getSeller() === $security->getUser()) {
            return $this->json(['message' => 'Cannot buy your own item.'], 400);
        }

        $sale = $doctrine->getRepository(Sale::class)->findOneBy([
            'buyer' => $security->getUser(),
            'item' => $item
        ]);

        if ($sale) {
            return $this->json(['message' => 'Already placed a buying request.'], 400);
        }

        $sale = new Sale();
        $sale->setItem($item)
            ->setBuyer($security->getUser())
            ->setStatus(Sale::STATUS_PENDING);

        $em = $doctrine->getManager();
        $em->persist($sale);
        $em->flush();

        return $this->json([], 201);
    }

    /**
     * @Route("/closeSale/{id}", name="sale_approve", methods={"POST"})
     */
    public function closeSale(ManagerRegistry $doctrine, Request $request, int $id, Security $security): Response
    {
        $requestData = json_decode($request->getContent(), true);

        if (!isset($requestData['status']) || !in_array($requestData['status'], [Sale::STATUS_SOLD, Sale::STATUS_REJECTED])) {
            return $this->json(['message' => 'Invalid request data.'], 400);
        }

        $sale = $doctrine->getRepository(Sale::class)->find($id);

        if (!$sale) {
            return $this->json(['message' => 'Sale not found.'], 400);
        }
        if ($sale->getItem()->getSeller() !== $security->getUser()) {
            return $this->json(['message' => 'Unable to approve sale for item not owned by you.'], 403);
        }

        $sale->setStatus($requestData['status']);

        $em = $doctrine->getManager();
        $em->persist($sale);
        $em->flush();

        return $this->json([], 200);
    }

    /**
     * @Route("/sale/{id}", name="sale_delete", methods={"DELETE"})
     */
    public function deleteSale(ManagerRegistry $doctrine, Request $request, int $id, Security $security): Response
    {
        $sale = $doctrine->getRepository(Sale::class)->find($id);

        if (!$sale) {
            return $this->json(['message' => 'Sale does not exist.'], 400);
        }

        if ($sale->getBuyer() !== $security->getUser()) {
            return $this->json(['message' => 'You can only delete your own buying requests.'], 403);
        }

        if ($sale->getStatus() != Sale::STATUS_PENDING) {
            return $this->json(['message' => 'Sale already closed.'], 400);
        }

        $em = $doctrine->getManager();
        $em->remove($sale);
        $em->flush();

        return $this->json([], 202);
    }
}
