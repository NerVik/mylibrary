<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
class Item
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    private ?string $author = null;

    #[ORM\Column]
    private ?int $price = null;

    #[ORM\Column(length: 64)]
    private ?string $bookCondition = null;

    #[ORM\ManyToOne(inversedBy: 'items')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $seller = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: WishlistedItem::class, orphanRemoval: true)]
    private Collection $wishlistedItems;

    #[ORM\OneToMany(mappedBy: 'item', targetEntity: Sale::class, orphanRemoval: true)]
    private Collection $sales;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->wishlistedItems = new ArrayCollection();
        $this->sales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getBookCondition(): ?string
    {
        return $this->bookCondition;
    }

    public function setBookCondition(string $bookCondition): self
    {
        $this->bookCondition = $bookCondition;

        return $this;
    }

    public function getSeller(): ?User
    {
        return $this->seller;
    }

    public function setSeller(?User $seller): self
    {
        $this->seller = $seller;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return Collection<int, WishlistedItem>
     */
    public function getWishlistedItems(): Collection
    {
        return $this->wishlistedItems;
    }

    public function addWishlistedItem(WishlistedItem $wishlistedItem): self
    {
        if (!$this->wishlistedItems->contains($wishlistedItem)) {
            $this->wishlistedItems->add($wishlistedItem);
            $wishlistedItem->setItem($this);
        }

        return $this;
    }

    public function removeWishlistedItem(WishlistedItem $wishlistedItem): self
    {
        if ($this->wishlistedItems->removeElement($wishlistedItem)) {
            // set the owning side to null (unless already changed)
            if ($wishlistedItem->getItem() === $this) {
                $wishlistedItem->setItem(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Sale>
     */
    public function getSales(): Collection
    {
        return $this->sales;
    }

    public function addSale(Sale $sale): self
    {
        if (!$this->sales->contains($sale)) {
            $this->sales->add($sale);
            $sale->setItem($this);
        }

        return $this;
    }

    public function removeSale(Sale $sale): self
    {
        if ($this->sales->removeElement($sale)) {
            // set the owning side to null (unless already changed)
            if ($sale->getItem() === $this) {
                $sale->setItem(null);
            }
        }

        return $this;
    }
}
